package com.infore.scrubber.activity;

import android.view.animation.AccelerateDecelerateInterpolator;

import com.eftimoff.androipathview.PathView;
import com.infore.module_common.widgets.BaseActivity;
import com.infore.scrubber.R;


public class SplashActivity extends BaseActivity {

    @Override
    protected void initial() {
        initView();
    }

    private void initView() {
        PathView pathView = findViewById(R.id.pathView);
        pathView.setFillAfter(true);

        pathView.getPathAnimator().delay(10).duration(1500)
                .listenerStart(null)
                .listenerEnd(() -> {
                    navigate(MainActivity.class);
                    finish();
                })
                .interpolator(new AccelerateDecelerateInterpolator())
                .start();
    }

    @Override
    protected int layoutResId() {
        return R.layout.activity_splash;
    }

}
