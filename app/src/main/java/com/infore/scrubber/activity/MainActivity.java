package com.infore.scrubber.activity;

import com.infore.module_common.widgets.BaseActivity;
import com.infore.scrubber.R;

public class MainActivity extends BaseActivity {


    @Override
    protected void initial() {

    }

    @Override
    protected int layoutResId() {
        return R.layout.activity_main;
    }
}
