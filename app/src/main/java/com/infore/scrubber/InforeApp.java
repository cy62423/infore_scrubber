package com.infore.scrubber;

import android.app.Application;

import com.infore.module_common.ActivityManager;
import com.infore.module_common.utils.SPUtil;

public class InforeApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        ActivityManager.getInstance().registerActivityLifecycleCallback(this);
        SPUtil.getInstance().initial(this);

        /**
         * todo
         * 没有历史任务时，不加载 {@link WorkStatusRecorder}
         */
        //WorkStatusRecorder.getInstance().setCurWorkingMap(MapBeanUtil.getLastWorkBean());
    }
}
