package com.infore.scrubber.fragment;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.infore.module_common.widgets.BaseFragment;
import com.infore.scrubber.R;


/**
 * 首页没有地图数据时的展示页面
 */
public class HomeEmptyDataFragment extends BaseFragment {

    public HomeEmptyDataFragment() {
    }

    public static HomeEmptyDataFragment newInstance() {
        return new HomeEmptyDataFragment();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.findViewById(R.id.main_confirm_tv).setOnClickListener(v -> {
            // 新建地图
            //navigate(NewMapActivity.class);
        });
    }

    @Override
    protected int layoutResId() {
        return R.layout.fragment_home_empty_data;
    }
}
