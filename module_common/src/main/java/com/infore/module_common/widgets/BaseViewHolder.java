package com.infore.module_common.widgets;

import android.content.Context;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class BaseViewHolder extends RecyclerView.ViewHolder {

    private View rootView;
    private SparseArray<View> view_cache;

    private List<Integer> viewClickIdList;

    public BaseViewHolder(@NonNull View itemView) {
        super(itemView);
        rootView = itemView;
        view_cache = new SparseArray<>();
    }

    public static BaseViewHolder getInstance(Context context, int layoutId, ViewGroup parent) {
        View rootView = LayoutInflater.from(context).inflate(layoutId, parent, false);
        return new BaseViewHolder(rootView);
    }

    @SuppressWarnings("unchecked")
    public <T extends View>T getView(int viewId) {
        View view = view_cache.get(viewId);
        if (view == null) {
            view = rootView.findViewById(viewId);
            view_cache.put(viewId, view);
        }

        return (T) view;
    }

    public void addViewClickId(int viewId) {
        if (viewClickIdList == null) {
            viewClickIdList = new ArrayList<>();
        }

        viewClickIdList.add(viewId);
    }

    protected List<Integer> getViewClickIdList() {
        return viewClickIdList;
    }
}
