package com.infore.module_common.widgets;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.infore.module_common.interfaces.OnItemClickListener;
import com.infore.module_common.interfaces.OnItemLongClickListener;
import com.infore.module_common.interfaces.OnSubItemClickListener;

import java.util.List;

public abstract class BaseAdapter<T> extends RecyclerView.Adapter<BaseViewHolder> {

    protected Context context;

    private int layoutId;
    protected List<T> dataList;

    private OnItemClickListener onItemClickListener;
    private OnSubItemClickListener onSubItemClickListener;
    private OnItemLongClickListener onItemLongClickListener;

    public BaseAdapter(Context context, int layoutId, List<T> dataList) {
        this.context = context;
        this.layoutId = layoutId;
        this.dataList = dataList;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return BaseViewHolder.getInstance(context, layoutId, parent);
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        convert(holder, dataList.get(position), position);

        if (onItemClickListener != null) {
            holder.itemView.setOnClickListener(view -> {
                onItemClickListener.onItemClick(holder.itemView, position);
            });
        }

        if (onItemLongClickListener != null) {
            holder.itemView.setOnLongClickListener(view -> {
                onItemLongClickListener.onItemLongClick(holder.itemView, position);
                return true;
            });
        }

        List<Integer> viewClickIdList = holder.getViewClickIdList();
        if (viewClickIdList != null) {
            for (Integer id : viewClickIdList) {
                View view = holder.getView(id);
                view.setOnClickListener(v -> {
                    if (onSubItemClickListener != null) {
                        onSubItemClickListener.onSubItemClickListener(holder.itemView, id, position);
                    }
                });
            }
        }
    }

    protected abstract void convert(BaseViewHolder holder, T data, int position);

    @Override
    public int getItemCount() {
        return dataList == null ? 0 : dataList.size();
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public void setOnSubItemClickListener(OnSubItemClickListener onSubItemClickListener) {
        this.onSubItemClickListener = onSubItemClickListener;
    }

    public void setOnItemLongClickListener(OnItemLongClickListener onItemLongClickListener) {
        this.onItemLongClickListener = onItemLongClickListener;
    }
}
