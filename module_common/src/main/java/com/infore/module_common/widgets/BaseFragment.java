package com.infore.module_common.widgets;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public abstract class BaseFragment extends Fragment {

    protected Context context;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        if (layoutResId() <= 0) {
            throw new IllegalArgumentException(
                    "layoutResId() return the layout resource id is invalidate.");
        }
        return inflater.inflate(layoutResId(), container, false);
    }

    /**
     * 页面跳转
     *
     * @param clazz 目标Activity.class
     */
    protected void navigate(Class<?> clazz) {
        navigate(clazz, null);
    }

    protected void navigate(Class<?> clazz, Bundle extras) {
        Intent intent = new Intent(context, clazz);
        if (extras != null) {
            intent.putExtras(extras);
        }

        startActivity(intent);
    }

    protected void navigateForResult(int requestCode, Class<?> clazz) {
        navigateForResult(requestCode, clazz, null);
    }

    protected void navigateForResult(int requestCode, Class<?> clazz, Bundle extras) {
        Intent intent = new Intent(context, clazz);
        if (extras != null)
            intent.putExtras(extras);
        ((Activity)context).startActivityForResult(intent, requestCode);
    }

    protected abstract int layoutResId();
}
