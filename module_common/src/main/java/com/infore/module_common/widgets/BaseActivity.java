package com.infore.module_common.widgets;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.infore.module_common.R;
import com.infore.module_common.interfaces.OnSubTitleMenuClickListener;

import java.util.ArrayList;
import java.util.List;

/**
 * BaseActivity
 */
public abstract class BaseActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TextView subTitleText;
    private List<OnSubTitleMenuClickListener> onSubTitleMenuClickListeners;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(layoutResId());
        initial();
    }

    /**
     * 页面跳转
     *
     * @param clazz 目标Activity.class
     */
    protected void navigate(Class<?> clazz) {
        navigate(clazz, null);
    }

    protected void navigate(Class<?> clazz, Bundle extras) {
        Intent intent = new Intent(this, clazz);
        if (extras != null) {
            intent.putExtras(extras);
        }

        startActivity(intent);
    }

    protected void navigateForResult(int requestCode, Class<?> clazz) {
        navigateForResult(requestCode, clazz, null);
    }

    protected void navigateForResult(int requestCode, Class<?> clazz, Bundle extras) {
        Intent intent = new Intent(this, clazz);
        if (extras != null)
            intent.putExtras(extras);

        startActivityForResult(intent, requestCode);
    }

    /**
     * 设置导航栏
     *
     * @param strId 标题字符串资源id
     */
    protected void setActionBar(int strId) {
        final String title = getString(strId);
        setActionBar(title);
    }

    protected void setActionBar(String title) {
        setActionBar(title, false);
    }

    protected void setActionBar(String title, boolean isSetBackIcon) {
        initToolbar(title, isSetBackIcon);
        if (subTitleText.getVisibility() == View.VISIBLE) {
            subTitleText.setVisibility(View.GONE);
        }

        setSupportActionBar(toolbar);
    }

    protected void setActionBar(String title, String subTitle, boolean isSetBackIcon) {
        initToolbar(title, isSetBackIcon);

        subTitleText.setText(subTitle);
        subTitleText.setVisibility(View.VISIBLE);
        subTitleText.setOnClickListener(v -> {
            for (OnSubTitleMenuClickListener menuClickListener : onSubTitleMenuClickListeners) {
                menuClickListener.onSubTitleMenuClickListener();
            }
        });

        setSupportActionBar(toolbar);
    }

    private void initToolbar(String title, boolean isSetBackIcon) {
        toolbar = findViewById(R.id.layout_toolbar);
        subTitleText = findViewById(R.id.toolbar_sub_title_tv);
        TextView titleText = findViewById(R.id.toolbar_title_tv);
        titleText.setText(title);
        if (isSetBackIcon) {
            toolbar.setNavigationIcon(R.drawable.icon_arrows_back);
            toolbar.setNavigationOnClickListener(v -> onBackPressed());
        }
    }

    protected TextView getSubTitleText() {
        return subTitleText;
    }

    protected void addOnSubTitleMenuClickListener(OnSubTitleMenuClickListener menuClickListener) {
        if (onSubTitleMenuClickListeners == null) {
            onSubTitleMenuClickListeners = new ArrayList<>();
        }
        onSubTitleMenuClickListeners.add(menuClickListener);
    }

    protected void removeOnSubTitleMenuClickListener(OnSubTitleMenuClickListener menuClickListener) {
        onSubTitleMenuClickListeners.remove(menuClickListener);
    }

    protected abstract void initial();

    protected abstract int layoutResId();
}
