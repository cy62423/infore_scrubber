package com.infore.module_common.utils;

import android.text.TextUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateFormatter {

    private static final String YYYY_MM_DD_HH_MM = "yyyy-mm-dd HH:mm";

    private static final SimpleDateFormat GHOST_FORMATTER = (SimpleDateFormat) DateFormat.getDateInstance();
    private static final DateFormat DEFAULT_FORMATTER = new SimpleDateFormat(YYYY_MM_DD_HH_MM, Locale.CHINA);

    public static String millis2Date(long millis) {
        return DEFAULT_FORMATTER.format(millis);
    }

    public static String millis2Date(long millis, String pattern) {
        if (TextUtils.isEmpty(pattern)) {
            throw new IllegalArgumentException("param pattern could not be null.");
        }

        GHOST_FORMATTER.applyPattern(pattern);
        return GHOST_FORMATTER.format(millis);
    }

    public static long date2Millis(String date, String pattern) {
        try {
            GHOST_FORMATTER.applyPattern(pattern);
            Date parse = GHOST_FORMATTER.parse(date);

            return parse.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
            return -1;
        }
    }

    public static String dateTransform(String date, String originPattern, String desPattern) {
        try {
            GHOST_FORMATTER.applyPattern(originPattern);
            Date parse = GHOST_FORMATTER.parse(date);

            GHOST_FORMATTER.applyPattern(desPattern);
            return GHOST_FORMATTER.format(parse);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }
}
