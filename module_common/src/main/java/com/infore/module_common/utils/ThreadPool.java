package com.infore.module_common.utils;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class ThreadPool {

    private static volatile ThreadPoolExecutor sNodeMainExecuteThreadPool;

    private static final int KEEP_ALIVE_TIME = 60;

    private static final int CPU_COUNT = Runtime.getRuntime().availableProcessors();

    private static final BlockingQueue<Runnable> sTaskQueue = new LinkedBlockingQueue<>();

    private static final ThreadFactory sThreadFactory = new ThreadFactory() {
        private final AtomicInteger count = new AtomicInteger(1);

        @Override
        public Thread newThread(Runnable task) {
            return new Thread(task, "infore #" + count.getAndIncrement());
        }
    };

    private ThreadPool() {
    }

    private static ThreadPoolExecutor nodeMainExecuteThreadPool() {
        if (sNodeMainExecuteThreadPool == null) {
            synchronized (ThreadPool.class) {
                if (sNodeMainExecuteThreadPool == null) {
                    sNodeMainExecuteThreadPool = new ThreadPoolExecutor(
                            CPU_COUNT + 1,
                            2 * CPU_COUNT,
                            KEEP_ALIVE_TIME, TimeUnit.MICROSECONDS,
                            sTaskQueue, sThreadFactory);
                }
            }
        }

        return sNodeMainExecuteThreadPool;
    }

    public static void execute(Runnable task) {
        nodeMainExecuteThreadPool().execute(task);
    }
}
