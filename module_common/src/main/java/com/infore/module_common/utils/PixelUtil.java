package com.infore.module_common.utils;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Point;
import android.os.Build;
import android.view.Display;
import android.view.WindowManager;

public class PixelUtil {

    private static final Point point = new Point();


    public static int dp2px(Context context, float dpValue) {
        return (int) ((float) ((int) context.getResources().getDisplayMetrics().density) * dpValue + 0.5f);
    }

    public static int sp2px(Context context, float spValue) {
        float fontScale = context.getResources().getDisplayMetrics().scaledDensity;
        return (int) (spValue * fontScale + 0.5F);
    }


    /**
     * 获取屏幕宽度
     *
     * @param context Activity
     * @return ScreenWidth
     */
    public static int getScreenWidth(Context context) {
        Display display = display(context);
        if (display != null) {
            display.getSize(point);
            return point.x;
        }
        return 0;
    }

    /**
     * 获取屏幕高度
     *
     * @param context Activity
     * @return ScreenHeight
     */
    public static int getScreenHeight(Context context) {
        Display display = display(context);
        if (display != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                display.getRealSize(point);

            } else {
                display.getSize(point);
            }
            //需要减去statusBar的高度  不用考虑navigationBar Display已经自动减去了
            return point.y - getStatusBarHeight((Activity) context);
        }

        return 0;
    }

    public static int getStatusBarHeight(Activity activity) {
        Resources resources = activity.getResources();
        int resourceId = resources.getIdentifier(
                "status_bar_height",
                "dimen",
                "android");

        return resources.getDimensionPixelSize(resourceId);
    }

    public static int getNavigationBarHeight(Activity activity) {
        Resources resources = activity.getResources();
        int resourceId = resources.getIdentifier(
                "navigation_bar_height",
                "dimen",
                "android");

        return resources.getDimensionPixelSize(resourceId);
    }

    private static Display display(Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        return wm.getDefaultDisplay();
    }
}
