package com.infore.module_common.utils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

/**
 * JSON 解析封装
 */
public class JsonFormatter {

    private Gson gson;

    private JsonFormatter() {
        gson = new Gson();
    }

    public String toJson(Object source) {
        return gson.toJson(source);
    }

    public <T> T fromJson(String json, Class<T> clazz) {
        return gson.fromJson(json, clazz);
    }

    public <T> T fromJsonWithType(String json) {
        final Type type = new TypeToken<T>() {
        }.getType();

        return gson.fromJson(json, type);
    }

    public static JsonFormatter getInstance() {
        return InstanceHolder.INSTANCE;
    }

    private static final class InstanceHolder {
        private static final JsonFormatter INSTANCE = new JsonFormatter();
    }
}
