package com.infore.module_common.utils;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;

public class PackageUtil {

    public static String getVersionName(Context context) {
        try {
            PackageInfo packageInfo = packageInfo(context);

            return packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static long getVersionCode(Context context) {
        try {
            PackageInfo packageInfo = packageInfo(context);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                return packageInfo.getLongVersionCode();
            }

            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        return -1;
    }

    public static String getAppName(Context context) {
        try {
            PackageInfo packageInfo = packageInfo(context);
            ApplicationInfo applicationInfo = packageInfo.applicationInfo;

            return applicationInfo.name;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        return null;
    }

    private static PackageInfo packageInfo(Context context) throws
            PackageManager.NameNotFoundException {
        PackageManager pm = context.getPackageManager();
        return pm.getPackageInfo(context.getPackageName(), 0);
    }
}
