package com.infore.module_common.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

public class SPUtil {

    private static final String SP_NAME = "infore";

    private static SharedPreferences sSP;
    private static SharedPreferences.Editor sEditor;

    private SPUtil() {

    }

    public static SPUtil getInstance() {
        return InstanceHolder.INSTANCE;
    }

    public void initial(Context context) {
        if (sEditor == null) {
            sSP = context.getSharedPreferences(SP_NAME, Context.MODE_PRIVATE);
            sEditor = sSP.edit();
        }
    }

    public void putString(String key, String content) {
        sEditor.putString(key, content);
        sEditor.apply();
    }

    public String getString(String key, String defaultValue) {
        return sSP.getString(key, defaultValue);
    }

    public <T> void putObject(String key, T object) {
        String objStr = JsonFormatter.getInstance().toJson(object);
        putString(key, objStr);
    }

    public <T>T getObject(String key) {
        String result = sSP.getString(key, "");
        if (TextUtils.isEmpty(result)) {
            return null;
        }

        return JsonFormatter.getInstance().fromJsonWithType(result);
    }

    public void putInt(String key, int value) {
        sEditor.putInt(key, value);
        sEditor.apply();
    }

    public int getInt(String key, int defaultValue) {
        return sSP.getInt(key, defaultValue);
    }

    public void putBoolean(String key, boolean value) {
        sEditor.putBoolean(key, value);
        sEditor.apply();
    }

    public boolean getBoolean(String key, boolean defaultValue) {
        return sSP.getBoolean(key, defaultValue);
    }

    private static final class InstanceHolder {
        private static final SPUtil INSTANCE = new SPUtil();
    }
}
