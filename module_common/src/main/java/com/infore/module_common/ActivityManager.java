package com.infore.module_common;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ActivityManager {

    //private List<Activity> activities;
    private List<WeakReference<Activity>> activities;

    private ActivityManager() {
        activities = new ArrayList<>();
    }

    public static ActivityManager getInstance() {
        return InstanceHolder.INSTANCE;
    }

    public void registerActivityLifecycleCallback(Application application) {
        application.registerActivityLifecycleCallbacks(activityLifecycle);
    }

    public void destroyAllActivity() {
        for (WeakReference<Activity> reference : activities) {
            Activity activity = reference.get();
            if (activity != null)
                activity.finish();
        }
    }

    private void removeActivity(Activity activity) {
        for (WeakReference<Activity> reference : activities) {
            Activity at = reference.get();
            if (at != null && at == activity) {
                activities.remove(reference);
                return;
            }
        }
    }

    private final Application.ActivityLifecycleCallbacks activityLifecycle = new Application.ActivityLifecycleCallbacks() {

        @Override
        public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
            WeakReference<Activity> reference = new WeakReference<>(activity);
            ActivityManager.this.activities.add(reference);
            //ActivityManager.this.activities.add(activity);
        }

        @Override
        public void onActivityStarted(Activity activity) {

        }

        @Override
        public void onActivityResumed(Activity activity) {

        }

        @Override
        public void onActivityPaused(Activity activity) {

        }

        @Override
        public void onActivityStopped(Activity activity) {

        }

        @Override
        public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

        }

        @Override
        public void onActivityDestroyed(Activity activity) {
            ActivityManager.this.removeActivity(activity);
            //ActivityManager.this.activities.remove(activity);
        }
    };

    private static final class InstanceHolder {
        private static final ActivityManager INSTANCE = new ActivityManager();
    }
}
