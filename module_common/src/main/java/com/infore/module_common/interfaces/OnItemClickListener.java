package com.infore.module_common.interfaces;

import android.view.View;

public interface OnItemClickListener {

    void onItemClick(View itemView, int position);
}
