package com.infore.module_common.interfaces;

import android.view.View;

public interface OnItemLongClickListener {

    void onItemLongClick(View itemView, int position);
}
