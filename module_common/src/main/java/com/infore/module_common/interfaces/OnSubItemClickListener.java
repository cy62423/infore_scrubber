package com.infore.module_common.interfaces;

import android.view.View;

public interface OnSubItemClickListener {

    void onSubItemClickListener(View view, int id, int position);
}
