package com.infore.module_net;

import android.text.TextUtils;
import android.util.Log;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;
import okio.ByteString;

/**
 * Http 网络请求实现类
 */
public class HttpRequest implements INetRequest<String, String>, INetSyncRequest<String, String> {

    /**
     * 同步请求
     *
     * @param url           请求地址
     * @param request_data  请求数据类型
     * @return 请求响应内容
     */
    @Override
    public String syncRequest(String url, String request_data) {

        return null;
    }

    /**
     * 异步请求
     *
     * @param url           请求地址
     * @param request_data  请求数据类型
     * @param callback      请求响应回调
     */
    @Override
    public void request(String url, String request_data, IRequestCallback<String> callback) {
        if (TextUtils.isEmpty(url))
            throw new NullPointerException("connect url can not be null.");

        final RequestBody requestBody = FormBody.create(
                MediaType.parse("application/json; charset=utf-8"),
                request_data);
        Request request = new Request.Builder().url(url).post(requestBody).build();
        mOkHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                callback.onFailed(e.getMessage());
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                callback.onSuccess(url, response.body().string());
            }
        });
    }

    /**
     * WebSocket
     *
     * @param url   url
     * @param data  发送内容
     */
    @Override
    public void ws_request(String url, String data) {
        if (TextUtils.isEmpty(url))
            throw new NullPointerException("connect url can not be null.");

        if (webSocket == null) {
            final Request request = new Request.Builder().url(url).build();
            webSocket = mOkHttpClient.newWebSocket(request, listener);
        }
        webSocket.send(data);
    }

    private final WebSocketListener listener = new WebSocketListener() {
        @Override
        public void onOpen(WebSocket webSocket, Response response) {
            super.onOpen(webSocket, response);
        }

        @Override
        public void onMessage(WebSocket webSocket, String text) {
            super.onMessage(webSocket, text);
        }

        @Override
        public void onMessage(WebSocket webSocket, ByteString bytes) {
            super.onMessage(webSocket, bytes);
        }

        @Override
        public void onClosing(WebSocket webSocket, int code, String reason) {
            super.onClosing(webSocket, code, reason);
        }

        @Override
        public void onClosed(WebSocket webSocket, int code, String reason) {
            super.onClosed(webSocket, code, reason);
        }

        @Override
        public void onFailure(WebSocket webSocket, Throwable t, Response response) {
            super.onFailure(webSocket, t, response);
        }
    };

    private static final String TAG = "HttpRequest";
    public static final String BASE_URL = "ws://10.53.6.194:8006";

    private static final int CONNECT_TIME_OUT = 60;
    private static final int READ_TIME_OUT = 60;
    private static final int WRITE_TIME_OUT = 60;
    private final OkHttpClient mOkHttpClient;
    private WebSocket webSocket;

    private HttpRequest(){
        mOkHttpClient = new OkHttpClient.Builder()
                .connectTimeout(CONNECT_TIME_OUT, TimeUnit.SECONDS)
                .readTimeout(READ_TIME_OUT, TimeUnit.SECONDS)
                .writeTimeout(WRITE_TIME_OUT, TimeUnit.SECONDS)
                .pingInterval(10, TimeUnit.SECONDS)
                .addInterceptor(chain -> {
                    Request request = chain.request();
                    Log.i(TAG, request.toString());
                    Response response = chain.proceed(request);
                    Log.i(TAG, response.toString());
                    return response;
                })
                .build();
    }

    public static HttpRequest getInstance() {
        return InstanceHolder.INSTANCE;
    }

    private static final class InstanceHolder {
        private static final HttpRequest INSTANCE = new HttpRequest();
    }
}
