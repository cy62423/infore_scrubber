package com.infore.module_net;

import android.os.Build;

import androidx.annotation.RequiresApi;

import com.infore.module_common.utils.JsonFormatter;
import com.infore.module_net.annotation.Param;
import com.infore.module_net.annotation.Path;
import com.infore.module_net.annotation.Type;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;

public class DynamicAgent {

    @SuppressWarnings("unchecked")
    public static <T> T getApi(Class<T> apiClass) {
        return (T) Proxy.newProxyInstance(apiClass.getClassLoader(), new Class[]{apiClass},
                (proxy, method, args) -> {
                    String path = HttpRequest.BASE_URL + method.getAnnotation(Path.class).path();
                    String request_type = method.getAnnotation(Type.class).type();
                    if ("webSocket".equals(request_type)) {
                        HttpRequest.getInstance().ws_request(path, (String)args[0]);
                        return null;
                    }

                    Map<String, Object> params = new HashMap<>();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        Parameter[] parameters = method.getParameters();
                        // callback 不处理
                        for (int i = 0; i < method.getParameterCount() - 1; i++) {
                            Parameter parameter = parameters[i];
                            Param param = parameter.getAnnotation(Param.class);
                            params.put(param.key(), args[i]);
                        }

                    } else {
                        Annotation[][] parameterAnnotations = method.getParameterAnnotations();
                        for (int i = 0; i < parameterAnnotations.length; i++) {
                            Annotation[] annotations = parameterAnnotations[i];
                            for (Annotation annotation : annotations) {
                                if (annotation.annotationType() == Param.class) {
                                    Param param = (Param) annotation;
                                    params.put(param.key(), args[i]);
                                }
                            }
                        }
                    }

                    // 取最后一项参数为回调函数
                    IRequestCallback<String> callback = (IRequestCallback<String>) args[args.length - 1];
                    HttpRequest.getInstance().request(path,
                            JsonFormatter.getInstance().toJson(params),
                            callback);

                    return null;
                });
    }
}
