package com.infore.module_net;

/**
 * 网络异步请求接口
 *
 * @param <D> 请求数据类型
 * @param <R> 请求响应回调数据类型
 */
public interface INetRequest<D, R> {

    /**
     * 网络请求
     *
     * @param url           请求地址
     * @param request_data  请求数据类型
     * @param callback      请求响应回调
     */
    void request(String url, D request_data, IRequestCallback<R> callback);

    void ws_request(String url, D data);
}
