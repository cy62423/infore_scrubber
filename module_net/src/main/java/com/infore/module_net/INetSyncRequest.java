package com.infore.module_net;

/**
 * 网络同步请求接口
 *
 * @param <D> 请求数据类型
 * @param <R> 请求响应回调数据类型
 */
public interface INetSyncRequest<D, R> {

    /**
     * 网络请求
     *
     * @param url           请求地址
     * @param request_data  请求数据类型
     */
    R syncRequest(String url, D request_data);
}
