package com.infore.module_net;

/**
 * 网络请求回调
 *
 * @param <R>
 */
public interface IRequestCallback<R> {

    /**
     * 网络请求成功回调
     *
     * @param url       请求地址
     * @param response  响应内容
     */
    void onSuccess(String url, R response);

    /**
     * 网络请求失败回调
     *
     * @param err_message 失败信息提示
     */
    void onFailed(String err_message);
}
